import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    Image,
    View,
} from 'react-native';
import { Icon } from 'react-native-elements'

import { ENV } from './Variables'
import deviceStorage from './deviceStorage'
import axios from 'axios'
import { withNavigation } from 'react-navigation'

class Login extends Component {
    static navigationOptions = {
        title: 'Arka Chat',
    };
    constructor() {
        super()
        this.state = {
            name: '',
            email: '',
            password: '',
            jwt: null,
            error: '',
            loading: false,
            securePassword: true
        };
    }
    handleUserLogin = (e) => {
        const { email, password } = this.state
        axios.post(`${ENV.url}/login`, {
            email, password
        })
            .then(response => {
                let jwt = response.data.token
                console.log(jwt)
                let userData = JSON.stringify(response.data.data)
                this.setState({ error: '', loading: true, jwt: jwt })
                if (jwt !== null) {
                    this.setState({
                        loading: false
                    })
                    deviceStorage.saveItem("token", response.data.token)
                    deviceStorage.saveItem("my_user", userData)
                    this.props.navigation.navigate('Chat', {
                        user_id: userData.id
                    })
                } else {
                    e.preventDefault()
                }

            })
            .catch(err => {
                this.setState({
                    error: 'Login Failed..',
                    loading: false
                })
                alert('Login Failed')
            })
    }


    onChangeText = email => this.setState({ email });
    onChangePass = password => this.setState({ password });

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: 'green' }}>
                <View style={{ flex: 1, backgroundColor: '#232833' }}>
                    <View style={{ flex: 1, backgroundColor: '#285BC0', borderBottomLeftRadius: 70, alignItems: 'center', justifyContent: 'center' }}>
                        <Image style={{
                            flex: 0.4,
                            alignSelf: 'stretch',
                            width: undefined,
                            height: undefined
                        }} resizeMode='contain' source={require('../assets/arkademy-logo.png')} />

                    </View>
                </View>
                <View style={{ flex: 1, backgroundColor: '#285BC0' }}>

                    <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1, backgroundColor: '#232833', borderTopRightRadius: 70 }}>
                        <View style={{ height: 30 }}>
                            <Text style={{ color: '#EAEDF3', fontSize: 18 }}>Login To Your Account</Text>
                        </View>
                        <View style={{ marginTop: 15, height: 140, justifyContent: 'space-around', width: '70%' }}>
                            <View style={{ borderRadius: 4, alignItems: 'center', flex: 0.25, backgroundColor: '#2E323E', flexDirection: 'row' }}>
                                <View style={{ height: '100%', justifyContent: 'center', flex: 0.2, borderRightWidth: 1 }}>
                                    <Icon type='antdesign' name='user' color="#EAEDF3" />
                                </View>
                                <View style={{ flex: 0.8 }}>
                                    <TextInput placeholder="email" placeholderTextColor='#EAEDF3'
                                        onChangeText={this.onChangeText}
                                        value={this.state.email}
                                        style={{ paddingLeft: 10, paddingTop: 10, alignItems: 'center', justifyContent: 'center', color: '#EAEDF3' }} />
                                </View>
                            </View>
                            <View style={{ borderRadius: 4, alignItems: 'center', flex: 0.25, backgroundColor: '#2E323E', flexDirection: 'row' }}>
                                <View style={{ height: '100%', justifyContent: 'center', flex: 0.25, borderRightWidth: 1 }}>
                                    <Icon type='material-community' name='lock-open-outline' color="#EAEDF3" />
                                </View>
                                <View style={{ flex: 0.8 }}>
                                    <TextInput placeholder="password" placeholderTextColor='#EAEDF3'
                                        secureTextEntry={this.state.securePassword}
                                        onChangeText={this.onChangePass}
                                        value={this.state.password}
                                        style={{ paddingLeft: 10, paddingTop: 10, alignItems: 'center', justifyContent: 'center', color: '#EAEDF3' }} />
                                </View>
                                <TouchableOpacity 
                                    style={{ height: '100%', justifyContent: 'center', flex: 0.2, borderRightWidth: 1 }}
                                    onPress={() => {this.setState({securePassword: !this.state.securePassword})}}>
                                    <View >
                                        <Icon size={20} type='simple-line-icon' name='eye' color="#EAEDF3" />
                                    </View>
                                </TouchableOpacity>
                            </View>
                            <View style={{ borderRadius: 4, alignItems: 'center', flex: 0.25, backgroundColor: '#2E323E', flexDirection: 'row' }}>
                                <TouchableOpacity
                                    style={{ backgroundColor: '#F47E00', flexDirection: 'row', alignItems: 'center', height: '100%', justifyContent: 'center', flex: 0.6 }}
                                    onPress={this.handleUserLogin}>
                                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                                        <Text style={{ color: '#EAEDF3' }}>Let's Go </Text>
                                        <Icon size={15} type='antdesign' name='arrowright' color="#EAEDF3" />
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity style={{ flex: 0.6, alignItems: 'center', justifyContent: 'center' }}>
                                    <View style={{ alignItems: 'center', justifyContent: 'center', flexDirection: 'row' }}>
                                        <Text style={{ color: '#EAEDF3' }}>forgot password ?</Text>
                                    </View>
                                </TouchableOpacity>

                            </View>

                        </View>
                    </View>

                </View>
            </View>
            // <View>
            //     <Text style={styles.title}>Email:</Text>
            //     <TextInput
            //         style={styles.nameInput}
            //         placeHolder="Mbah Download"
            //         onChangeText={this.onChangeText}
            //         value={this.state.email}
            //     />
            //     <Text style={styles.title}>Password:</Text>
            //     <TextInput
            //         secureTextEntry={true}
            //         style={styles.nameInput}
            //         placeHolder="aku sayang kamu"
            //         onChangeText={this.onChangePass}
            //         value={this.state.password}
            //     />
            //     <TouchableOpacity onPress={this.handleUserLogin}>
            //         <Text style={styles.buttonText}>Login</Text>
            //     </TouchableOpacity>
            // </View>
        );
    }
}

const offset = 24;
const styles = StyleSheet.create({
    title: {
        marginTop: offset,
        marginLeft: offset,
        fontSize: offset,
    },
    nameInput: {
        height: offset * 2,

        margin: offset,
        paddingHorizontal: offset,
        borderColor: '#111111',
        borderWidth: 1,
    },
    buttonText: {
        marginLeft: offset,
        fontSize: offset,
    },
});

export default withNavigation(Login);