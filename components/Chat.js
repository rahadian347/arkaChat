import React, { Component } from "react";
import { Button, Keyboard, AsyncStorage, View, Text, TouchableOpacity, StyleSheet, ScrollView, FlatList, TextInput } from 'react-native'
import axios from 'axios'
import { ENV } from './Variables'
import ThumbnailPhoto from "./ThumbnailPhoto";
import { withNavigation } from 'react-navigation'
import { Overlay, Icon } from 'react-native-elements'
import GestureRecognizer, { swipeDirections } from 'react-native-swipe-gestures';


class Chat extends Component {
    signOutAsync = async () => {
        await AsyncStorage.clear();
        this.props.navigation.navigate('AuthLoading');
    };

    //Get state from Login
    static navigationOptions = ({ navigation }) => ({
        title: (navigation.state.params || {}).name || 'Chat!',
        header: null
    });

    constructor() {
        super()
        this.state = {
            name: '',
            messages: [],
            text: '',
            jwt: '',
            keyboardOffset: 0,
            user: {},
            isVisible: false
        };


    }

    componentDidMount() {
        this.interval = setInterval(() => {
            this.getChat()
        }, 500);
        this.keyboardDidShowListener = Keyboard.addListener(
            'keyboardDidShow',
            this._keyboardDidShow,
        );
        this.keyboardDidHideListener = Keyboard.addListener(
            'keyboardDidHide',
            this._keyboardDidHide,
        );
        this.props.navigation.setParams({ handleLogout: this.signOutAsync });
    }
    componentWillUnmount() {
        clearInterval(this.interval);
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }

    getChat = async () => {
        const jwt = await AsyncStorage.getItem('token');
        const user = await AsyncStorage.getItem('my_user', (err, item) => item);
        this.setState({
            jwt: jwt,
            user: user
        })
        const headers = {
            'Authorization': 'Bearer ' + jwt
        };
        let { data: messages } = await axios.get(`${ENV.url}/chats`, {
            headers: headers
        })
        this.setState({
            messages: messages
        })
    }

    signOutAsync = async () => {
        await AsyncStorage.clear();
        this.props.navigation.navigate('AuthLoading');
    };
    scrollToEnd = () => {
        this.scrollView.scrollToEnd();
    }

    handlePost = () => {

        const headers = {
            'Authorization': 'Bearer ' + this.state.jwt
        };
        const user = JSON.parse(this.state.user)

        let data = {
            text: this.state.text,
            user_id: user.id
        }
        axios.post(`${ENV.url}/chats`, data, {
            headers: headers
        })
            .then(response => {
                // this.props.navigation.navigate('AuthLoading')
            })
            .catch(err => {
                console.log("send data failed")
                alert('post failed')
            })

        this.scrollToEnd()
        this.setState({ text: '' })
    }

    render() {
        let chats = this.state.messages
        return (
            <View style={{
                flex: 1, justifyContent: 'space-between', backgroundColor: '#F4F6F8'
            }}>
                <View style={{
                    width: "100%", flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', backgroundColor: '#F4F6F8', shadowColor: "#000",
                }}>
                    <Text style={{ color: '#F47E00', fontSize: 20, fontWeight: '500', paddingLeft: 20 }}>ArkaChat</Text>
                    <TouchableOpacity onPress={() => { this.signOutAsync() }} style={{ paddingRight: 10 }}>
                        <Icon
                            name='exit-to-app'
                            type='material'
                            color='#F47E00'
                            size={30} />
                    </TouchableOpacity>
                </View>

                <View style={{ paddingTop: 20, paddingHorizontal: 10, width: "100%", flex: 6, backgroundColor: '#232833', borderTopLeftRadius: 30, borderTopRightRadius: 30 }}>
                    <ScrollView ref={(scrollView) => { this.scrollView = scrollView }}>
                        <FlatList
                            data={chats}
                            renderItem={({ item }) => <ChatList user={this.state.user} data={item} />}
                            keyExtractor={(item) => item.id.toString()}
                        />
                    </ScrollView>
                </View>
                <Overlay isVisible={this.state.isVisible}>
                    <Text>Hello from Overlay!</Text>
                </Overlay>


                <View style={{ backgroundColor: '#232833', alignItems: 'center', justifyContent: "center", flexDirection: 'row', flex: 1 }}>
                    <View style={{ flexDirection: 'row', backgroundColor: 'white', borderRadius: 50, height: 50, borderColor: 'gray', borderWidth: 1 }}>
                        <TextInput
                            style={{
                                paddingLeft: 20,
                                width: '75%',
                                bottom: this.state.keyboardOffset,
                            }}

                            onChangeText={(text) => this.setState({ text })}
                            value={this.state.text}
                            placeholder="Send a chat.."
                        />
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <TouchableOpacity onPress={() => this.handlePost()} style={{ alignItems: "center", justifyContent: "center", borderRadius: 50, height: 25, backgroundColor: '#F47E00', width: 60 }}>
                                <View style={{}}>
                                    <Text style={{ color: '#EAEDF3', flexWrap: "wrap" }}>Send</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}



// Flatlist Component


class ChatList extends Component {

    constructor(props) {
        super(props);
        this.state = {

        };
    }

    render() {
        let data = this.props.data
        let userId = JSON.parse(this.props.user)
        if (data.User.id == userId.id) {
            return (
                <TouchableOpacity style={{flex: 1}}>
                    <View style={{ marginTop:3,paddingLeft: 40,flex: 1,alignItems: 'center', justifyContent: "center", flexDirection: 'row', justifyContent: "flex-end" }}>
                        <View style={{ paddingVertical: 5, paddingHorizontal: 5, backgroundColor: "#3766BD", borderRadius: 10, alignItems: "center", justifyContent: "center" }} >
                            <Text style={{ color: 'white', }}>{data.text}</Text>
                        </View>
                        <ThumbnailPhoto style={{ width: 35, height: 35, padding: 5 }} characterImageThumb={data.User.avatar} />
                    </View >
                </TouchableOpacity>
            )
        } else {
            return (
                <View style={{ alignItems: 'center', justifyContent: "center", flexDirection: 'row', justifyContent: "flex-start" }}>
                    <ThumbnailPhoto style={{ width: 35, height: 35, padding: 5 }} characterImageThumb={data.User.avatar} />
                    <View style={{ paddingVertical: 5, paddingHorizontal: 5, backgroundColor: "#F4F6F8", borderRadius: 10, alignItems: "center", justifyContent: "center" }} >
                        <Text style={{ color: '#6E7278', }}>{data.text}</Text>
                    </View>
                </View >
            )
        }
    }

}

export default withNavigation(Chat)

