import Login from './Login';
import Chat from './Chat';
import AuthLoading from './AuthLoading';
import Test from './Test';

// Import React Navigation
import { createStackNavigator, createAppContainer, createSwitchNavigator } from 'react-navigation'

// Create the navigator
const AppStack = createStackNavigator(

    {
        Chat: {
            screen: Chat,
            navigationOptions: { gesturesEnabled: true }
        }
    },
    {
        defaultNavigationOptions: {
            initialRouteName: Chat,
            resetOnBlur: true
        }
    }
);

const AuthStack = createStackNavigator(
    {
        Login: {
            screen: Login,
            navigationOptions: { gesturesEnabled: false}
        }
    },
    {
        defaultNavigationOptions: {
            resetOnBlur: true,
            header: null
        }
    }
)

const AppContainer = createAppContainer(createSwitchNavigator(
    {
        AuthLoading: AuthLoading,
        Auth: AuthStack,
        App: AppStack,
        Test: Test
    },
    {

        initialRouteName: 'AuthLoading',
        resetOnBlur: true,
    }
));



export default createAppContainer(AppContainer);